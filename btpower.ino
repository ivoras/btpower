String device_name = "BTPOWER01";
String device_version = "000";
String device_type = "BTPOWER";

#define N_RELAYS 1
#define RELAY_STATE_ON 0
#define RELAY_STATE_OFF 1

char relay_states[N_RELAYS] = { 0 }; // relay state (0 = off)
char relay_pins[N_RELAYS] = { 8 }; // relay pins

void setup()
{
  int i;
  for (i = 0; i < N_RELAYS; i++) {
    pinMode(relay_pins[i], OUTPUT);
    digitalWrite(relay_pins[i], RELAY_STATE_OFF);
  }
  
  Serial.begin(9600); //set baud rate
  /* Wait purely cosmetically for initial hw setup, then reset the device name */
  delay(5000);
  
  Serial.println("AT");
  Serial.readStringUntil('\n');
  Serial.print("AT+NAME" + device_name);
  Serial.readStringUntil('\n');
  
  Serial.setTimeout(1000);
}

void loop()
{
  String message; // complete incoming message
  String cmd, param; // only the command part
  
  message = Serial.readStringUntil('\n');
  message.trim();
  
  if (message.startsWith("BTP")) { // this is actually "BTP" followed by any character, like "+"
    int p;
    cmd = message.substring(4);
    p = cmd.indexOf(' ');
    if (p == -1)
      param = "";
    else {
      param = cmd.substring(p + 1);
      cmd = cmd.substring(0, p);
    }
    //Serial.println("CMD: " + cmd + " PARAM: " + param);
    if (cmd == "RON") { // relay ON, param = relay No.
      int rn = param.toInt();
      if (rn < N_RELAYS) {
        relay_states[rn] = 1;
        digitalWrite(relay_pins[rn], RELAY_STATE_ON);
        Serial.println("+OK");
      } else
        Serial.println("+ERR");
    } else if (cmd == "ROFF") { // relay OFF; param = relay No.
      int rn = param.toInt();
      if (rn < N_RELAYS) {
        relay_states[rn] = 0;
        digitalWrite(relay_pins[rn], RELAY_STATE_OFF);
        Serial.println("+OK");
      } else
        Serial.println("+ERR");
    } else if (cmd == "GETR") { // get relay state (0 = off)
      int rn = param.toInt();
      if (rn < N_RELAYS)
        Serial.println("+OK " + String((int)(relay_states[rn])));
      else
        Serial.println("+ERR");
    } else if (cmd == "GETNR") { // get number of relays
      Serial.println("+OK " + String(N_RELAYS));
    } else if (cmd == "GETNAME") {
      Serial.println("+OK " + device_name);
    } else if (cmd == "GETVER") {
      Serial.println("+OK " + device_version);
    } else if (cmd == "GETTYPE") {
      Serial.println("+OK " + device_type);
    } else
      Serial.println("+ERR " + cmd);
  } else if (message.length() > 0)
    Serial.println("+ERR " + device_type);
}
    
